﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LAB3KPZ
{
   public class Resource
    {
        public int ResourceID { get; set; }
        public int TypeID { get; set; }
        public string Name { get; set; }
        public double Cost { get; set; }
    }

   public class Type
    {
        public string Name { get; set; }
        public int ID { get; set; }
    }

    public class ModelTests
    {

        public static List<Type> Types
        {
            get
            {
                return new List<Type>()
                {
                new Type(){Name="Furniture", ID=01},
                new Type(){ Name="Machinery", ID=02},
                new Type(){ Name="Stationery", ID=03},

                    };
            }
        }

        public static List<Resource> Resources
        {
            get
            {
                return new List<Resource>()
                {
                   new Resource{ResourceID = 01, Name="ofice chair",TypeID = 01,Cost = 15 },
                    new Resource{ResourceID = 02, Name="chair",TypeID = 01,Cost = 10},
                     new Resource{ResourceID = 03, Name="table",TypeID = 01,Cost = 40},
                      new Resource{ResourceID = 04, Name="board",TypeID = 01,Cost = 15},
                       new Resource{ResourceID = 06, Name="wardrobe",TypeID = 01,Cost = 80},
                        new Resource{ResourceID = 07, Name="monitor",TypeID = 02,Cost = 70},
                         new Resource{ResourceID = 08, Name="monitor",TypeID = 02,Cost = 70},
                          new Resource{ResourceID = 09, Name="computer",TypeID = 02,Cost = 400},
                           new Resource{ResourceID = 10, Name="projector",TypeID = 02,Cost = 150},
                            new Resource{ResourceID = 11, Name="A4 paper",TypeID = 03,Cost = 20}
                };
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            
            int[] Array = TypesToArray();
            foreach(int i in Array)
            {
                Console.WriteLine("Type ID: {0}", i);
            }
            InformationJoinQuery();
            InformationQuery();
            InformationChangeQuery();

            Console.ReadKey();
        }

        static void InformationQuery()
        {
             
            List<Resource> r1 = ModelTests.Resources;

            Console.WriteLine("\nInformation about resources sorted by cost:");

            var result = r1.OrderBy(c => c,
                Comparer<Resource>.Create((c1, c2) =>
                {
                    return c1.Cost.CompareTo(c2.Cost);
                }));
            foreach (var res in result)
            {
                Console.WriteLine("ID:{0,3} |Name: {1, 13} |Type: {2 , 3} |Cost: {3, 4}$", res.ResourceID, res.Name, res.TypeID, res.Cost);
            }

            var newlist = from res in r1.OfType<Resource>()
                          where res.Name.StartsWith("c")
                          orderby res.Name
                          select res;
            Console.WriteLine("\nInformation about resources starting with 'c':");
            foreach (var res in newlist)
            {
                Console.WriteLine("ID:{0,3} |Name: {1, 13} |Type: {2 , 3} |Cost: {3, 4}$", res.ResourceID, res.Name, res.TypeID, res.Cost);
            }

            Console.WriteLine();

           
        }


        static int[] TypesToArray() 
        {
            
            List<Type> t = ModelTests.Types;
            int[] nums = new int [t.Count];
            int it = 0;
            foreach (var i in t)
            {
                nums[it] = i.ID;
                it++;
            }

            var r = from num in nums
                    select num;

            Console.WriteLine();

            var newlist = r.ToList<int>();
            var newarray = newlist.ToArray();
            


            return newarray;

        }

        static void InformationChangeQuery()
        {
            Console.WriteLine("\nEdit list");
            List<Resource> r1 = ModelTests.Resources;
            Console.WriteLine("Enter ResourceID:");
            int idr = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter Name:");
            var n = Console.ReadLine();
            Console.WriteLine("Enter TypeID:");
            var idt = Console.ReadLine();
            Console.WriteLine("Enter cost:");
            var c = Console.ReadLine();

            var v = new { ResourceID = idr, Name = n, TypeID = idt, Cost = c };

            var rchange = from res in r1.OfType<Resource>()
                             where res.ResourceID == v.ResourceID
                             select new {v.ResourceID, v.Name , v.TypeID, v.Cost };
            
            foreach (var res in rchange)
            {
                Console.WriteLine("\nID:{0,3} |Name: {1, 13} |Type: {2 , 3} |Cost: {3, 4}$", res.ResourceID,
                    res.Name, res.TypeID, res.Cost);
            }
            

        }
        static void InformationJoinQuery()
        {
            List<Resource> resources = ModelTests.Resources;
            List<Type> types = ModelTests.Types;

            var innerJoin =
               from r in resources
               join t in types on r.TypeID
               equals t.ID
               select new { ResourceID = r.ResourceID, ResourceName = r.Name, TypeName = t.Name, Cost = r.Cost };

            foreach (var i in innerJoin)
            {
                Console.WriteLine("ID:{0,3} |Name: {1, 13} |Type: {2 , 13} |Cost: {3, 4}$",
                    i.ResourceID, i.ResourceName, i.TypeName, i.Cost);
            }
        }
    }  
}

