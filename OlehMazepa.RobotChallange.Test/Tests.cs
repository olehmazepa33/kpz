﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;
using OlehMazepa.RobotChallange;


namespace OlehMazepa.RobotChallange.Test
{
    [TestClass]
    public class Tests
    {
        [TestMethod]
        public void TestDistance()
        {
            var p1 = new Position(1, 1);
            var p2 = new Position(3, 3);

            Assert.AreEqual(8, DistanceHelper.FindDistance(p1, p2));
            Assert.AreNotEqual(9, DistanceHelper.FindDistance(p1, p2));
          

        }

        [TestMethod]
        public void TestDistance2()
        {
            var p1 = new Position(2, 2);
            var p2 = new Position(3, 3);

            Assert.IsTrue(2 == DistanceHelper.FindDistance(p1, p2));
            Assert.IsFalse(7 == DistanceHelper.FindDistance(p1, p2));

        }

        [TestMethod]
        public void TestMoveStationCommandNULL()
        {
            var algorithm = new OlehMazepaAlgorithm();
            var map = new Map();
            var stationPosition = new Position(1, 1);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPosition, RecoveryRate = 10 });
            var robots = new List<Robot.Common.Robot>()
                { new Robot.Common.Robot() {Energy = 0, Position = new Position (3,3) } };

            var command = algorithm.DoStep(robots, 0, map);

            Assert.IsTrue(command is MoveCommand);
            Assert.AreNotEqual(((MoveCommand)command).NewPosition, stationPosition);
        }

        [TestMethod]
        public void TestMoveStationCommand()
        {
            var algorithm = new OlehMazepaAlgorithm();
            var map = new Map();
            var stationPosition = new Position(1, 1);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPosition, RecoveryRate = 10 });
            var robots = new List<Robot.Common.Robot>()
                { new Robot.Common.Robot() {Energy = 200, Position = new Position (3,3) } };

            var command = algorithm.DoStep(robots, 0, map);

            Assert.IsTrue(command is MoveCommand);
            Assert.AreEqual(((MoveCommand)command).NewPosition, stationPosition);
        }

        [TestMethod]
        public void TestCollectNULL()
        {
            var map = new Map();
            var stationPos = new Position(1, 1);
            map.Stations.Add(new EnergyStation() { Energy = 0, Position = stationPos, RecoveryRate = 10 });

            var robots = new List<Robot.Common.Robot>()
                { new Robot.Common.Robot() {Energy = 200, Position = new Position (1, 1) } };

            var algorithm = new OlehMazepaAlgorithm();
            var command = algorithm.DoStep(robots, 0, map);
            Assert.IsTrue(command is CollectEnergyCommand);
        }

        [TestMethod]
        public void TestCollect()
        {
            var map = new Map();
            var stationPos = new Position(1, 1);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPos, RecoveryRate = 10 });

            var robots = new List<Robot.Common.Robot>()
                { new Robot.Common.Robot() {Energy = 200, Position = new Position (1, 1) } };

            var algorithm = new OlehMazepaAlgorithm();
            var command = algorithm.DoStep(robots, 0, map);
            Assert.IsTrue(command is CollectEnergyCommand);
        }

        [TestMethod]
        public void TestIsStationFree()
        {
            var map = new Map();
            var robots = new List<Robot.Common.Robot>()
                { new Robot.Common.Robot() {Energy = 200, Position = new Position (3, 3) } };
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = new Position(1,1) , RecoveryRate = 100 });
            var rob = new Robot.Common.Robot() { Energy = 200, Position = new Position(2, 2) };
            var algorithm = new OlehMazepaAlgorithm();

            Assert.IsTrue(algorithm.IsStationFree(map.Stations[0], rob, robots));
        }

        [TestMethod]
        public void TestCreate()
        {
            var map = new Map();
            var stationPos = new Position(1, 1);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPos, RecoveryRate = 100 });

            var robots = new List<Robot.Common.Robot>()
                { new Robot.Common.Robot() {Energy = 1000, Position = new Position (1, 1) } };

            var algorithm = new OlehMazepaAlgorithm();
            RobotCommand  command = algorithm.DoStep(robots, 0, map);
            Assert.IsTrue(command is CreateNewRobotCommand);
        }

        [TestMethod]
        public void TestMoveCommandNULL()
        {
            var algorithm = new OlehMazepaAlgorithm();
            var robots = new Robot.Common.Robot()
            { Energy = 0, Position = new Position(3, 3) };
            var p1 = new Position(1, 1);
            Position command = MoveRobotCommand(robots, p1);

            Assert.IsTrue(command is Position);
            Assert.AreEqual(p1, command);
        }

        [TestMethod]
        public void TestMoveCommand()
        {
            var algorithm = new OlehMazepaAlgorithm();       
            var robots = new Robot.Common.Robot()
                 {Energy = 200, Position = new Position (3,3) };
            var p1 = new Position(1, 1);
            Position command = MoveRobotCommand(robots, p1);

            Assert.IsTrue(command is Position);
            Assert.AreEqual(p1, command);
        }

        public Position MoveRobotCommand(Robot.Common.Robot rob, Position pos)
        {
                return rob.Position = pos;
        }
    }
    
}
