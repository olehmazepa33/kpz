﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace CSh_Events
{
    class ChangingArgs : EventArgs
    {
        public string Old { get; set; }
        public string New { get; set; }

    }
    
    class Program
    {
        public delegate void ChangeValueDel(object sender, ChangingArgs args);

        public static void ChangeNameMethod(object sender, ChangingArgs args)
        {
            Console.WriteLine("Item name was changed: from {0} to {1}", args.Old, args.New);
        }
        public static void ChangeIDMethod(object sender, ChangingArgs args)
        {
            Console.WriteLine("Item ID was changed: from {0} to {1}", args.Old, args.New);
        }
        public static void ChangePriceMethod(object sender, ChangingArgs args)
        {
            Console.WriteLine("Item price was changed: from {0} to {1}", args.Old, args.New);
        }
        public static void ChangeQualityMethod(object sender, ChangingArgs args)
        {
            Console.WriteLine("Item quality was changed: from {0} to {1}", args.Old, args.New);
        }

        static void Main(string[] args)
        {
            List<Item> Items = new List<Item>();
            Items.Add(new Item("Armor", "0000", 29.75, "Rare"));
            Items.Add(new Item("Knife", "0001", 3.5, "Common"));
            Items.Add(new Item("Sword", "0002", 213.0, "Legendary"));
            string input;
            Console.WriteLine();
            do
            {
                int i = 1;
                foreach (var Item in Items)
                {
                    Console.Write($"Item {i} | ");
                    Item.getInfo();
                    i++;
                }

                Console.WriteLine(
                    "\nDo you wish to change something? (tab N to exit)");
                input = Console.ReadLine();
                if (Int32.TryParse(input, out int itemIndex) && itemIndex <= Items.Count)
                {
                    Console.WriteLine("Enter new name: ");
                    Items[itemIndex - 1].ChangingEvent += new ChangeValueDel(ChangeNameMethod);
                    Items[itemIndex - 1].setName(Console.ReadLine());
                    Console.WriteLine("Enter new ID: ");
                    Items[itemIndex - 1].ChangingEvent -= ChangeNameMethod;
                    Items[itemIndex - 1].ChangingEvent += new ChangeValueDel(ChangeIDMethod);
                    Items[itemIndex - 1].setID(Console.ReadLine());
                    Console.WriteLine("Enter new price: ");
                    Items[itemIndex - 1].ChangingEvent -= ChangeIDMethod;
                    Items[itemIndex - 1].ChangingEvent += new ChangeValueDel(ChangePriceMethod);
                    double newPrice = Convert.ToDouble(Console.ReadLine());
                    Items[itemIndex - 1].setPrice(newPrice);
                    Console.WriteLine("Enter new quality: ");
                    Items[itemIndex - 1].ChangingEvent -= ChangePriceMethod;
                    Items[itemIndex - 1].ChangingEvent += new ChangeValueDel(ChangeQualityMethod);
                    Items[itemIndex - 1].setQuality(Console.ReadLine());
                }
            } while (input != "N");

            Console.WriteLine("Click any key to exit");
            Console.ReadKey();
        }

        public static object Item { get; set; }
    }

    class Item
    {
        private string Name;
        private string ID_Item;
        private double Price;
        private string Quality;

        public event Program.ChangeValueDel ChangingEvent;

        public Item()
        {
            this.Name = "----------";
            this.ID_Item = "--------";
            this.Price = 0;
            this.Quality = "--------";
        }
        public Item(string Name, string ID_Item, double Price, string Quality)
        {
            this.Name = Name;
            this.ID_Item = ID_Item;
            this.Price = Price;
            this.Quality = Quality;
        }
        public void setName(string Name)
        {
            ChangingEvent?.Invoke(this, new ChangingArgs() { New = Name, Old = this.Name });
            this.Name = Name;
        }
        public void setID(string ID_Item)
        {
            ChangingEvent?.Invoke(this, new ChangingArgs() { New = ID_Item, Old = this.ID_Item });
            this.ID_Item = ID_Item;
        }
        public void setPrice(double Price)
        {
            ChangingEvent?.Invoke(this, new ChangingArgs() { New = Price.ToString(), Old = this.Price.ToString() });
            this.Price = Price;
        }
        public void setQuality(string Quality)
        {
            ChangingEvent?.Invoke(this, new ChangingArgs() { New = Quality, Old = this.Quality });
            this.Quality = Quality;
        }
        public void getInfo()
        {
            Console.WriteLine("Name: {0, 10},\tID_Item: {1, 10},\tPrice: {2,5}, Quality : {3 , 4}", this.Name, this.ID_Item, this.Price, this.Quality);
        }
    }
}
