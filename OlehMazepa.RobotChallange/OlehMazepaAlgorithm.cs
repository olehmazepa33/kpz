﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Robot.Common;

namespace OlehMazepa.RobotChallange
{
    public class OlehMazepaAlgorithm : IRobotAlgorithm
    {
        
        public Position FindNearestFreeStation(Robot.Common.Robot movingRobot, Map map, IList<Robot.Common.Robot> robots)
        {
            EnergyStation nearest = null;
            int minDistance = int.MaxValue;
            foreach (var station in map.Stations)
            {
                if (IsStationFree(station, movingRobot, robots))
                {
                    int d = DistanceHelper.FindDistance(station.Position, movingRobot.Position);
                    if (d < minDistance)

                    {

                        minDistance = d;
                        nearest = station;

                    }
                }
            }
            return nearest == null ? null : nearest.Position;
        }
        public bool IsStationFree(EnergyStation station, Robot.Common.Robot movingRobot,
        IList<Robot.Common.Robot> robots)
        {
            return IsCellFree(station.Position, movingRobot, robots);
        }
        public bool IsCellFree(Position cell, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {
            foreach (var robot in robots)
            {
                if (robot != movingRobot)
                {
                    if (robot.Position == cell)
                        return false;
                }
            }
            return true;
        }

       
        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            Robot.Common.Robot movingRobot = robots[robotToMoveIndex];
            if ((movingRobot.Energy > 500) && (robots.Count < map.Stations.Count + 5))
            {
                return new CreateNewRobotCommand() { NewRobotEnergy = 200 };
            }

            Position stationPosition = FindNearestFreeStation(robots[robotToMoveIndex], map, robots);


            if (stationPosition == null)
                return null;

            if (stationPosition == movingRobot.Position)
                return new CollectEnergyCommand();
            else
            {
                
                Position newPosition = stationPosition;
                int distance = DistanceHelper.FindDistance(stationPosition, movingRobot.Position);
                if(distance > 40 || movingRobot.Energy > 17)
                {
                    int dx = Math.Sign(stationPosition.X - movingRobot.Position.X) * Math.Min(Math.Abs(stationPosition.X - movingRobot.Position.X), 2);
                    int dy = Math.Sign(stationPosition.Y - movingRobot.Position.Y) * Math.Min(Math.Abs(stationPosition.Y - movingRobot.Position.Y), 2);
                    newPosition = new Position(movingRobot.Position.X + dx, movingRobot.Position.Y + dy);
                }
                
                else if (movingRobot.Energy == 0)
                {
                    newPosition = movingRobot.Position;
                }
                return new MoveCommand() { NewPosition = newPosition };
            }
        }

        /*  public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
          {
              var myRobot = robots[robotToMoveIndex];

              var newposition = myRobot.Position;
              newposition.X = newposition.X + 1;
              newposition.Y = newposition.Y + 1;
              return new MoveCommand() { NewPosition = newposition };
          } */

        public string Author
        {
            get { return "Mazepa Oleh"; }
        }

        public string Description
        {
            get { return "Lab 1"; }
        }

        
    }
}
